package br.com.webtask.aula.controller.service;

import br.com.webtask.aula.domain.model.Task;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;
//@DataJpaTest
@SpringBootTest
class TaskServiceTest {
@Autowired
private TaskService tasks;
    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }
    @Test
    public void test(){
        Task tarefa = new Task(1l,"",
                LocalDate.now(),null,null);
       try {
           tasks.finalizar(tarefa);
       }catch (Exception e ){
           System.out.println(e.getMessage());
       }
    }
    @Test
    public void test2(){
        Task tarefa = new Task(1l,"dormir",
                LocalDate.now(),null,null);
        try {
            tasks.finalizar(tarefa);
        }catch (Exception e ){
            System.out.println(e.getMessage());
        }
    }
}